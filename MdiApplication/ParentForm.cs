﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MdiApplication
{
    public partial class ParentForm : Form
    {
        private int openDocuments = 0;

        public ParentForm()
        {
            InitializeComponent();
        }

        private void ExitMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void WindowCascadeMenuItem_Click(object sender, EventArgs e)
        {
            cascade();
        }

        private void WindowTileMenuItem_Click(object sender, EventArgs e)
        {
            tile();
        }

        private void NewMenuItem_Click(object sender, EventArgs e)
        {
            newDoc();
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Tag.ToString())
            {
                case "NewDoc":
                    newDoc();
                    break;
                case "Cascade":
                    cascade();
                    break;
                case "Title":
                    tile();
                    break;
            }
        }


        private void newDoc()
        {
            ChildForm newChild = new ChildForm();
            newChild.Text = newChild.Text + " " + ++openDocuments;
            newChild.MdiParent = this;
            newChild.Show();
        }
        private void cascade()
        {
            this.LayoutMdi(MdiLayout.Cascade);
        }

        private void tile()
        {
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }
    }
}